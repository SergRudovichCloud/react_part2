import { Chat } from "./src/components/index";
import rootReducer from "./src/rootReduser"

export default {
    Chat,
    rootReducer
};