import React, { Component } from 'react';
import "./message_input.scss";
import { sendMessage } from '../chat/actions';
import { connect } from "react-redux";

class MessageInput extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
        this.handleSend = this.handleSend.bind(this);
    }

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }

    handleSend() {
        this.setState({ value: '' });
        this.props.sendMessage(this.state.value);
    }
    render() {
        return (
            <div className="message-input">
                <textarea
                    className="message-input-text"
                    value={this.state.value}
                    onChange={this.handleChange}
                ></textarea>
                <button
                    className="message-input-button"
                    onClick={this.handleSend}
                >Send</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chatList.chat.messages
    }
}

const mapDispatchToProps = {
    sendMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);