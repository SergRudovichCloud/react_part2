import React, { Component } from 'react';
import { MessageItem } from '../index';
import './message.scss';

export default class Message extends Component {
    constructor(props) {
        super(props);
        this.state = { isLike: "message-like" };
    }

    handleClick = () => {
        if (this.state.isLike === "message-like") {
            this.setState({ isLike: "message-liked" })
        } else {
            this.setState({ isLike: "message-like" })
        }
    }
    render() {
        return (
            <div className="message">
                <div>
                    <div className="message-user-avatar">
                        <img src={this.props.message.avatar} alt="avatar" />
                    </div>
                    <div className="message-user-name">{this.props.message.user}</div>
                </div>
                <div className="message-body">
                    <MessageItem message={this.props.message} />
                    <button
                        className={this.state.isLike}
                        onClick={this.handleClick}
                    >Like</button>
                </div>
            </div>
        )
    }
}
