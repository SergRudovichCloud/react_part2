import React, { Component } from 'react';
import './edit-modal.scss';
import { connect } from "react-redux";
import { hideModal, editMessage } from '../chat/actions';

class EditModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
        this.onSave = this.onSave.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onSave() {
        let editedMessage = this.props.message;
        editedMessage.text = this.state.value;
        editedMessage.editedAt = Date.now();
        this.props.editMessage(editedMessage);
        this.props.hideModal();
        this.setState({ value: '' });
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    getModalContent() {
        if (this.state.value === '') this.setState({ value: this.props.message.text });
        return (
            <div className="edit-message-modal modal-shown">
                <div className="modal-header">
                    <button className="edit-message-close" onClick={this.props.hideModal}>x</button>
                </div>
                <div className="edit-message-wrapper">
                    <textarea
                        className="edit-message-input"
                        value={this.state.value}
                        onChange={this.handleChange}
                    ></textarea>
                    <button
                        className="edit-message-button"
                        onClick={this.onSave}
                    >Save</button>
                </div>
            </div>
        )
    }

    render() {
        const iseditModal = this.props.editModal;
        return iseditModal ? this.getModalContent() : null
    }

}

const mapStateToProps = (state) => {
    return {
        editModal: state.chatList.chat.editModal,
        message: state.chatList.chat.editMessage
    }
}

const mapDispatchToProps = {
    hideModal,
    editMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);