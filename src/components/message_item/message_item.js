import React, { Component } from 'react';
import moment from 'moment';
import './message_item.scss';

export default class MessageItem extends Component {
    render() {
        return (
            <div>
                <div className="message-time">{moment(this.props.message.createdAt).format('HH:mm')}</div>
                <div className="message-text">{this.props.message.text}</div>
            </div>
        )
    }
}
