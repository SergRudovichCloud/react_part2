import React, { Component } from 'react';
import { getUrl } from "../../helpers/";
import { connect } from "react-redux";
import { loadData } from './actions';
import './chat.scss';
import {
    MessageInput,
    MessageList,
    Header,
    EditModal,
    Preloader
} from '../index';

class Chat extends Component {

    getData = (data) => {
        this.props.loadData(data);
    }

    componentDidMount() {
        getUrl(this.props.url, this.getData);
    }

    render() {
        if (!this.props.preloader) {
        return (
            <div className="chat">
                <div className='logo'>Logo</div>
                <Header
                />
                <MessageList
                />
                <EditModal />
                <MessageInput
                    onMessageSend={this.handleMessageSend}
                />
            </div>
        )} else {
            return (
                <Preloader />
            )
        }
    }
}
const mapStateToProps = (state) => {
    return {
        preloader: state.chatList.chat.preloader,
    }
}

const mapDispatchToProps = {
    loadData
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);