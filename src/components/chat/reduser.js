
import {
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    SEND_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL,
    LOAD_DATA
} from './actionTypes';
import { USER_ID, USER_NAME } from '../../config';

const initialState = {
    chat: {
        messages: [],
        editModal: false,
        preloader: true,
        messagesCount: 0,
        users: 0,
        lastMessageData: '2020-07-16T19:48:56.273Z',
        userMesgCount: 0,
        editMessage: null,
        lastOwnMessageId: 0
    }
}

export default function chatList(state = initialState, action) {
    switch (action.type) {
        case LOAD_DATA: {
            const { data } = action.payload;
            const fetchUsers = new Set();
            data.forEach(message => {
                fetchUsers.add(message.userId);
            });
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messages: [...data],
                    preloader: false,
                    messagesCount: data.length,
                    users: fetchUsers.size,
                    lastMessageData: data[data.length - 1]?.createdAt
                }
            }
        }
        case DELETE_MESSAGE: {
            const { id } = action.payload;
            const result = state.chat.messages.filter(message => message.id !== id);
            let newUser = 0;
            if (state.chat.userMesgCount === 1) newUser = -1;
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messages: [...result],
                    messagesCount: state.chat.messagesCount - 1,
                    users: state.chat.users + newUser,
                    userMesgCount: state.chat.userMesgCount - 1
                }
            }
        }
        case EDIT_MESSAGE: {
            const { message } = action.payload;
            const index = state.chat.messages.findIndex(el => el.id === message.id);
            const result = state.chat.messages;
            result[index] = message;
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messages: [...result],
                }
            }
        }
        case SHOW_MODAL: {
            const { id } = action.payload;
            const message = state.chat.messages.filter(message => message.id === id)
            return {
                ...state,
                chat: {
                    ...state.chat,
                    editModal: true,
                    editMessage: message[0],
                }
            }
        }
        case HIDE_MODAL: {
            return {
                ...state,
                chat: {
                    ...state.chat,
                    editModal: false,
                }
            }
        }
        case SEND_MESSAGE: {
            const { text } = action.payload;
            const newMessage = {
                id: USER_ID + new Date(),
                userId: USER_ID,
                avatar: "",
                user: USER_NAME,
                text: text,
                createdAt: Date.now(),
                editedAt: ""
            }
            let newUser = 0;
            if (state.chat.userMesgCount === 0) newUser += 1;
            return {
                ...state,
                chat: {
                    ...state.chat,
                    messages: [...state.chat.messages, newMessage],
                    messagesCount: state.chat.messagesCount + 1,
                    users: state.chat.users + newUser,
                    lastMessageData: newMessage.createdAt,
                    userMesgCount: state.chat.userMesgCount + 1,
                    lastOwnMessageId: newMessage.id
                }
            }
        }
        default:
            return state
    }
}


