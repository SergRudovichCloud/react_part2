import {
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    SEND_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL,
    LOAD_DATA
} from './actionTypes';

export const loadData = data => ({
    type: LOAD_DATA,
    payload: {
        data
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const editMessage = message => ({
    type: EDIT_MESSAGE,
    payload: {
        message
    }
});

export const sendMessage = text => ({
    type: SEND_MESSAGE,
    payload: {
        text
    }
});

export const hideModal = () => ({
    type: HIDE_MODAL,
});

export const showModal = id => ({
    type: SHOW_MODAL,
    payload: {
        id
    }
});