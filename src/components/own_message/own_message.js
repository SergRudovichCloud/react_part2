import React, { Component } from 'react';
import { MessageItem } from '../index';
import './own_message.scss';

export default class OwnMessage extends Component {
    handleEdit = () => {
        this.props.onMessageEdit(this.props.message.id);
    }

    handleDelete = () => {
        this.props.onMessageDelete(this.props.message.id);
    }

    render() {
        if (this.props.lastOwnMessageId === this.props.message.id){
                    return (
            <div className="own-message">
                <MessageItem message={this.props.message} />
                <div className="controls">
                    <button
                        className="message-edit"
                        onClick={this.handleEdit}
                    >Edit</button>
                    <button
                        className="message-delete"
                        onClick={this.handleDelete}
                    >Delete</button>
                </div>

            </div>
        )
        }else {
            return (
                <div className="own-message">
                    <MessageItem message={this.props.message} />
                    <div className="controls">
                        <button
                            className="message-delete"
                            onClick={this.handleDelete}
                        >Delete</button>
                    </div>
    
                </div>
            )
        }

    }
}