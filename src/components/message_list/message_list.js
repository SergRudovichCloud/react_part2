import React, { Component } from 'react';
import { OwnMessage, Message } from '../index';
import { USER_ID } from '../../config';
import './message_list.scss';
import { connect } from "react-redux";
import { deleteMessage, editMessage, showModal } from '../chat/actions';

class MessageList extends Component {
    render() {
        return (
            <div className="message-list">
                {this.props.messages.map(message => {
                    if (message.userId === USER_ID) {
                        return <OwnMessage
                            key={message.id}
                            message={message}
                            lastOwnMessageId={this.props.lastOwnMessageId}
                            onMessageDelete={this.props.deleteMessage}
                            onMessageEdit={this.props.showModal}

                        />
                    } else {
                        return <Message key={message.id}
                            message={message}
                        />
                    }

                })}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chatList.chat.messages,
        lastOwnMessageId: state.chatList.chat.lastOwnMessageId,
    }
}

const mapDispatchToProps = {
    editMessage,
    deleteMessage,
    showModal
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);