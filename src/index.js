import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
import Chat from './bsa';
import { MESSAGE_URL } from './config';
import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "./rootReduser";

const store = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Chat url={MESSAGE_URL} />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

