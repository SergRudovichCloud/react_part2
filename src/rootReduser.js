import { combineReducers } from "redux"
import chatList from "./components/chat/reduser"

const rootReducer = combineReducers({
    chatList
})

export default rootReducer;